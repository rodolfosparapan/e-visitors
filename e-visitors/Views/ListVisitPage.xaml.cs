﻿using System;
using System.Collections.Generic;
using evisitors.Models;
using evisitors.ViewModels;
using Xamarin.Forms;

namespace evisitors.Views
{
    public partial class ListVisitPage : ContentPage
    {
        public ListVisitViewModel ViewModel { get; set; }

        public ListVisitPage()
        {
            InitializeComponent();
            this.ViewModel = new ListVisitViewModel();
            BindingContext = this.ViewModel;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            SubscribeMessages();
            LoadVisitList();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            UnSubscribeMessages();
        }

        private async void LoadVisitList()
        {
            await this.ViewModel.LoadVisitList();
        }

        private void SubscribeMessages()
        {
            MessagingCenter.Subscribe<ListVisitViewModel, MessageData>(this, "VisitMessage", (viewModel, objMessage) => {
                DisplayAlert("Informação", objMessage.Message, "OK");
                if (objMessage.Success)
                    LoadVisitList();
            });

            MessagingCenter.Subscribe<ListVisitViewModel, Visit>(this, "GoToViewVisitPage", (viewModel, visit) => {
                var page = new CrudVisitPage(visit);
                page.SetOnlyView();
                Navigation.PushAsync(page);
            });

            MessagingCenter.Subscribe<ListVisitViewModel, Visit>(this, "GoToEditVisitPage", (viewModel, visit) => {
                Navigation.PushAsync(new CrudVisitPage(visit));
            });

            MessagingCenter.Subscribe<ListVisitViewModel>(this, "GoToAddVisitPage", (viewModel) => {
                Navigation.PushAsync(new CrudVisitPage());
            });   
        }

        private void UnSubscribeMessages()
        {
            MessagingCenter.Unsubscribe<ListVisitViewModel, MessageData>(this, "VisitMessage");
            MessagingCenter.Unsubscribe<ListVisitViewModel, Visit>(this, "GoToViewVisitPage");
            MessagingCenter.Unsubscribe<ListVisitViewModel, Visit>(this, "GoToEditVisitPage");
            MessagingCenter.Unsubscribe<ListVisitViewModel>(this, "GoToAddVisitPage");    
        }
    }
}
