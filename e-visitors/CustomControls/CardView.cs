﻿using System;
using Xamarin.Forms;

namespace evisitors.CustomControls
{
    public class CardView : Frame
    {
        public CardView()
        {
            OutlineColor = Color.Transparent;
            BackgroundColor = Color.WhiteSmoke;
            //CornerRadius = 5;
            Margin = new Thickness(0, 5);

            if (Device.RuntimePlatform == Device.iOS)
            {
                HasShadow = false;
            }

            if (Device.RuntimePlatform == Device.Android)
            {
                HasShadow = true;
            }
        }
    }
}
