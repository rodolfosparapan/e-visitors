﻿using Realms;
using System;
using System.Collections.Generic;
using System.Text;

namespace evisitors.Models
{
    public class Reason : RealmObject
    {
        [PrimaryKey]
        public int Id { get; set; }
        public string Description { get; set; }
        public int Index { get; set; }
        public string DtSync { get; set; }

    }
}
