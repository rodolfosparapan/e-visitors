﻿using System;
using System.Collections.Generic;
using evisitors.CrossDevice;
using evisitors.Models;
using evisitors.ViewModels;
using Plugin.Notifications;
using Xamarin.Forms;

namespace evisitors.Views
{
    public partial class ListSchedulePage : ContentPage
    {
        public ListScheduleViewModel ViewModel { get; set; }

        public ListSchedulePage()
        {
            InitializeComponent();
            this.ViewModel = new ListScheduleViewModel();
            BindingContext = this.ViewModel;
            LoadScheduleList();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<ListScheduleViewModel, String>(this, "ScheduleMessage", (viewModel, message) => {
                DisplayAlert("Informação", message, "OK");
            });
            MessagingCenter.Subscribe<ListScheduleViewModel, ScheduleWeek>(this, "ViewScheduleMessage", (viewModel, Schedule) => {
                Navigation.PushAsync(new ViewSchedulePage(Schedule));
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<ListScheduleViewModel, String>(this, "ScheduleMessage");
            MessagingCenter.Unsubscribe<ListScheduleViewModel, ScheduleWeek>(this, "ViewScheduleMessage");
        }

        private async void LoadScheduleList()
        {
            await this.ViewModel.LoadScheduleList();
            CrossNotification.ShowTest();
        }

        void Handle_ValueChanged(object sender, SegmentedControl.FormsPlugin.Abstractions.ValueChangedEventArgs e)
        {
            DayShedule.IsVisible = false;
            WeekShedule.IsVisible = false;
            MonthShedule.IsVisible = false;

            switch (e.NewValue)
            {
                case 0:
                    DayShedule.IsVisible = true;
                    break;
                case 1:
                    WeekShedule.IsVisible = true;
                    break;
                case 2:
                    MonthShedule.IsVisible = true;
                    break;
            }
        }
    }
}
