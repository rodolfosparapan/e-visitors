﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using evisitors.Models;
using evisitors.Services;
using evisitors.Services.MockData;
using Xamarin.Forms;

namespace evisitors.ViewModels
{
    public class ListVisitViewModel : BaseListViewModel<Visit>
    {
        private List<Visit> modelListFull;

        private string search;
        public string Search
        {
            get { return search; }
            set
            {
                search = value;
                this.ModelList.Clear();
                if (!String.IsNullOrEmpty(search))
                {
                    var filteredQuotes = this.modelListFull.Where(v => v.Client.Name.ToLower().Contains(search.ToLower())).ToList();
                    filteredQuotes.ForEach(this.ModelList.Add);
                }
                else
                {
                    this.modelListFull.ForEach(this.ModelList.Add);
                }
            }
        }

        public ListVisitViewModel()
        {
            this.HttpService = new VisitMockData();
            this.ModelList = new ObservableCollection<Visit>();
            this.BindCommands();
        }

        private void BindCommands()
        {
            this.EditCommand = new Command<Visit>((visit) => {
                MessagingCenter.Send<ListVisitViewModel, Visit>(this, "GoToEditVisitPage", visit);
            });

            this.DeleteCommand = new Command<Visit>(async (visit) => {
                if (await this.HttpService.DeleteAsync(visit))
                    MessagingCenter.Send<ListVisitViewModel, MessageData>(this, "VisitMessage", new MessageData() { Success = true, Message = "Visita excluida com sucesso!" });
                else
                    MessagingCenter.Send<ListVisitViewModel, MessageData>(this, "VisitMessage", new MessageData() { Success = false, Message = "Não foi possível excluir a visita!" });
            });

            this.AddCommand = new Command(() => {
                MessagingCenter.Send<ListVisitViewModel>(this, "GoToAddVisitPage");
            });

            this.ViewCommand = new Command<Visit>((visit) => {
                MessagingCenter.Send<ListVisitViewModel, Visit>(this, "GoToViewVisitPage", visit);
            });
        }

        public async Task LoadVisitList()
        {
            this.ModelList.Clear();
            this.Loading = true;
            this.modelListFull = await this.HttpService.ListAsync();
            this.modelListFull.ForEach(this.ModelList.Add);
            this.Loading = false;
        }
    }
}
