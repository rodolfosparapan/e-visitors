﻿using System;
using System.Net.Http;
using System.Text;
using evisitors.Models;
using evisitors.Models.HttpModels;
using Newtonsoft.Json;

namespace evisitors.Services.ParseData
{
    public class VisitParser : IModelParser<Visit, VisitHttp>
    {
        public StringContent ToJSON(Visit visit)
        {
            var json = JsonConvert.SerializeObject(new
            {
                codvisita = visit.CodVisit,
                dtvisita_plan = visit.ScheduleDate.ToString("yyyy-MM-dd"),
                dtvisita_real = visit.PerformedDate.ToString("yyyy-MM-dd"),
                codcliente = visit.Client.Id,
                codmotivo = visit.VisitReason.CodReason,
                motivo = visit.VisitReason.Reason,
                codresultado = visit.VisitResult.CodResult,
                resultado = visit.VisitResult.Result,
                proposta = visit.Proposal.Description,
                contato = visit.Contact.Name,
                depto = visit.Contact.Department,
                telcontato = visit.Contact.Phone,
                vlestimado = visit.Proposal.Value,
                latitude = visit.Geolocation.Latitude,
                longitude = visit.Geolocation.Longitude
             });

            return new StringContent(json, Encoding.UTF8, "application/json");   
        }

        public Visit ToModel(VisitHttp visitHttp)
        {
            return new Visit()
            {
                CodVisit = visitHttp.codvisita,
                ScheduleDate = Convert.ToDateTime(visitHttp.dtvisita_plan),
                PerformedDate = Convert.ToDateTime(visitHttp.dtvisita_real),

                Client = new Client() { 
                    Id = visitHttp.codcliente },

                VisitReason = new VisitReason() { 
                    CodReason = visitHttp.codmotivo, 
                    Reason = visitHttp.motivo},

                VisitResult = new VisitResult() { 
                    CodResult = visitHttp.codresultado, 
                    Result = visitHttp.resultado},

                Proposal = new Proposal(){ 
                    Description = visitHttp.proposta, 
                    Value = visitHttp.vlestimado },

                Contact = new Contact(){ 
                    Name = visitHttp.contato, 
                    Department = visitHttp.depto, 
                    Phone = visitHttp.telcontato},

                Geolocation = new Geolocation(){ 
                    Latitude = visitHttp.latitude, 
                    Longitude = visitHttp.longitude }
            };
        }
    }
}