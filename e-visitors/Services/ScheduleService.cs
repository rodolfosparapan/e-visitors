﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using evisitors.Models;

namespace evisitors.Services
{
    public class ScheduleService : BaseService, IService<ScheduleWeek>
    {
        public Task<bool> AddAsync(ScheduleWeek item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteAsync(ScheduleWeek item)
        {
            throw new NotImplementedException();
        }

        public Task<List<ScheduleWeek>> ListAsync()
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateAsync(ScheduleWeek item)
        {
            throw new NotImplementedException();
        }
    }
}
