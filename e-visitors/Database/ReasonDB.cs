﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using SQLite;
using Xamarin.Forms;
using evisitors.Models;
using Realms;

namespace evisitors.Database
{
    public class ReasonDB 
    {

        private Realm db;

        public ReasonDB()
        {
            db = Helpers.Util.GetInstanceRealm();
        }

        public List<Reason> GetReasons()
        {
            return db.All<Reason>().ToList();
        }


        public Reason GetOneReason(int pID)
        {

            return db.All<Reason>().FirstOrDefault(r => r.Id == pID);

        }

        public void InsertReason(Reason reason)
        {
            db.Write(() =>{
                db.Add(reason, false);
            });            
        }

        public void UpdateReason(Reason reason)
        {
            db.Write(() => {
                db.Add(reason, true);
            });
        }

        public void DeleteReason(Reason reason)
        {
            db.Write(() => {
                db.Remove(reason);
            });
        }


        public void SetAllReason(List<Reason> reasons)
        {

            db.Write(() =>
            {
                foreach (Reason r in reasons)
                {
                    //db.Remove(r);
                    db.Add(r, true);
                }
            });
        }

    }
}