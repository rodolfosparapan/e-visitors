﻿using System;
using System.Net.Http;

namespace evisitors.Services.ParseData
{
    interface IModelParser<Model, ModelHttp>
    {
        StringContent ToJSON(Model model);
        Model ToModel(ModelHttp modelHttp);
    }
}