﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace evisitors.Views
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();  
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            MessagingCenter.Send<LoginPage>(this, "SuccessLogin");
        }
    }
}
