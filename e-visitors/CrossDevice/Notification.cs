﻿using System;
using Plugin.Notifications;
using Xamarin.Forms;

namespace evisitors.CrossDevice
{
    public static class CrossNotification
    {
        public async static void ShowTest()
        {
            var notification = new Notification() { Id = 1, Title = "e-Visitors!", Message = "Vamos lá Jovem, Temos uma visita pela Frente!" };
            await CrossNotifications.Current.Send(notification);
        }
    }
}

