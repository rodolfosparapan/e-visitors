﻿using System;
using System.Threading.Tasks;
using evisitors.Exceptions;
using Plugin.Media;
using Plugin.Media.Abstractions;

namespace evisitors.CrossDevice
{
    public static class Camera
    {
        public async static Task<MediaFile> TakePhoto()
        {
            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
                throw new CameraException();

            var takePhotoOptions = new StoreCameraMediaOptions()
            {
                SaveToAlbum = true,
                PhotoSize = PhotoSize.Small
            };

            return await CrossMedia.Current.TakePhotoAsync(takePhotoOptions);
        }
    }
}
