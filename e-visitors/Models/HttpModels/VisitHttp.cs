﻿using System;
namespace evisitors.Models.HttpModels
{
    public class VisitHttp
    {
        public int codvisita;
        public string dtvisita_plan;
        public string dtvisita_real;
        public int codcliente;
        public int codmotivo;
        public string motivo;
        public int codresultado;
        public string resultado;
        public string realizada;
        public string dtrealizada;
        public string proposta;
        public string contato;
        public string depto;
        public string telcontato;
        public double vlestimado;
        public string dtcadastro;
        public string dtalteracao;
        public string latitude;
        public string longitude;
        public string dtsinc;
    }
}
