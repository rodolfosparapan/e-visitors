﻿using System;
using System.Windows.Input;
using evisitors.Services;

namespace evisitors.ViewModels
{
    public class BaseCrudViewModel<Model> : BaseViewModel
    {
        protected IService<Model> HttpService;
        protected bool IsAddOperation;
        public ICommand SaveCommand { get; protected set; }
        public ICommand EditCommand { get; protected set; }

        #region Proprierts
        private string pageTitle;
        public string PageTitle
        {
            get { return pageTitle; }
            set
            {
                pageTitle = value;
                OnPropertyChanged();
            }
        }

        private bool isFildsEnable = true;
        public bool IsFildsEnable
        {
            get { return isFildsEnable; }
            set
            {
                isFildsEnable = value;
                OnPropertyChanged();
            }
        }

        private bool onlyView = false;
        public bool OnlyView
        {
            get { return onlyView; }
            set
            {
                onlyView = value;
                OnPropertyChanged();
            }
        }
        #endregion Proprierts

        public BaseCrudViewModel()
        {
        }

        public void SetOnlyView()
        {
            IsFildsEnable = false;
            OnlyView = true;
        }
    }
}
