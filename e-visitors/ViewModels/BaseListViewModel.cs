﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using evisitors.Services;

namespace evisitors.ViewModels
{
    public class BaseListViewModel<Model> : BaseViewModel
    {
        protected IService<Model> HttpService;

        public ObservableCollection<Model> ModelList { get; set; }

        public ICommand AddCommand { get; protected set; }
        public ICommand EditCommand { get; protected set; }
        public ICommand DeleteCommand { get; protected set; }
        public ICommand ViewCommand { get; protected set; }

        private bool loading = false;
        public bool Loading
        {
            get { return loading; }
            set { loading = value; OnPropertyChanged(); }
        }

        public BaseListViewModel()
        {
        }
    }
}
