﻿using System;
using System.Collections.Generic;
using evisitors.Models;
using evisitors.ViewModels;
using Xamarin.Forms;

namespace evisitors.Views
{
    public partial class ViewSchedulePage : ContentPage
    {
        public ViewSchedulePage(ScheduleWeek Schedule)
        {
            InitializeComponent();
            BindingContext = new ViewScheduleViewModel(Schedule);
        }
    }
}
