﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace evisitors.Services.MockData
{
    public abstract class BaseMockService<Model> : IService<Model>
    {
        protected static List<Model> ModelList;
        protected static bool AlreadyLoaded = false;
        protected static int IncrementID = 1;

        public abstract Task<bool> AddAsync(Model model);
        public abstract Task<bool> UpdateAsync(Model model);

        public async Task<bool> DeleteAsync(Model model)
        {
            ModelList.Remove(model);
            return await Task.FromResult(true);
        }

        public async Task<List<Model>> ListAsync()
        {
            return await Task.FromResult(ModelList);
        }
    }
}
