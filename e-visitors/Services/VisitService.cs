﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using evisitors.Models;
using evisitors.Models.HttpModels;
using evisitors.Services.ParseData;
using Newtonsoft.Json;

namespace evisitors.Services
{
    public class VisitService : BaseService, IService<Visit>
    {
        private VisitParser parser;

        public VisitService()
        {
            this.parser = new VisitParser();
        }

        public async Task<bool> AddAsync(Visit visit)
        {
            var resposta = await this.httpClient.PostAsync(serverUri + "add", this.parser.ToJSON(visit));
            return resposta.IsSuccessStatusCode;
        }

        public async Task<bool> UpdateAsync(Visit visit)
        {
            var resposta = await this.httpClient.PutAsync(serverUri + "edit", this.parser.ToJSON(visit));
            return resposta.IsSuccessStatusCode;
        }

        public async Task<bool> DeleteAsync(Visit visit)
        {
            var resposta = await this.httpClient.DeleteAsync(serverUri + "del?codvisita=" + visit.CodVisit);
            return resposta.IsSuccessStatusCode;
        }

        public async Task<List<Visit>> ListAsync()
        {
            var serverResult = await this.httpClient.GetStringAsync(serverUri + "get");
            var visitJson = JsonConvert.DeserializeObject<VisitHttp[]>(serverResult);

            var visitList = new List<Visit>();
            foreach (VisitHttp visitHttp in visitJson)
            {
                visitList.Add(this.parser.ToModel(visitHttp));
            }

            return visitList;
        }
    }
}
