﻿using System;
namespace evisitors.Models
{
    public class Geolocation
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }
    }
}
