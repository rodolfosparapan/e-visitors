﻿using System;
using System.Collections.Generic;
using evisitors.Models;
using Xamarin.Forms;

namespace evisitors.Views
{
    public partial class MasterDetailView : MasterDetailPage
    {
        public List<MasterPageItem> menuItem { get; set; }
        public MasterDetailView()
        {
            InitializeComponent();

            menuItem = new List<MasterPageItem>(){
                new MasterPageItem { Title = "Agenda", Icon = "today.png", TargetType = typeof(ListSchedulePage) },
                new MasterPageItem { Title = "Visitas", Icon = "room.png", TargetType = typeof(ListVisitPage) },
                new MasterPageItem { Title = "Reembolsos", Icon = "money.png", TargetType = typeof(MoneyBackPage) },
                new MasterPageItem { Title = "Clientes", Icon = "mood.png", TargetType = typeof(ListClientPage) },
                new MasterPageItem { Title = "Sincronização", Icon = "sync.png", TargetType = typeof(SyncPage) }
            };
            navigationDrawerList.ItemsSource = menuItem;

            Detail = new NavigationPage((Page) Activator.CreateInstance(typeof(ListSchedulePage)));
            //Detail = new NavigationPage(new SearchPageSample());
        }

        private void OnMenuItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = (MasterPageItem)e.SelectedItem;
            Type page = item.TargetType;
            Detail = new NavigationPage((Page)Activator.CreateInstance(page));
            IsPresented = false;
        }
    }
}
