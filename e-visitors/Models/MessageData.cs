﻿using System;
namespace evisitors.Models
{
    public class MessageData
    {
        public string Title;
        public string Message;
        public bool Success;
    }
}
