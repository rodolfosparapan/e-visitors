﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using evisitors.Helpers;
using evisitors.Models;
using evisitors.Services.MockData;
using Xamarin.Forms;

namespace evisitors.ViewModels
{
    public class ListScheduleViewModel : BaseListViewModel<Visit>
    {
        public ObservableCollection<ScheduleWeek> ScheduleWeekList { get; private set; }
        public ObservableCollection<ScheduleMonth> ScheduleMonthList { get; private set; }

        public ListScheduleViewModel()
        {
            this.HttpService = new VisitMockData();
            this.ModelList = new ObservableCollection<Visit>();
            this.ScheduleWeekList = new ObservableCollection<ScheduleWeek>();
            this.ScheduleMonthList = new ObservableCollection<ScheduleMonth>();
            this.BindCommands();
        }

        private void BindCommands()
        {
            this.EditCommand = new Command<Visit>((visit) => {
                MessagingCenter.Send<ListScheduleViewModel, String>(this, "ScheduleMessage", "Comando para Editar Agenda!"); 
            });

            this.DeleteCommand = new Command<Visit>((visit) => {
                MessagingCenter.Send<ListScheduleViewModel, String>(this, "ScheduleMessage", "Comando para Excluir Agenda!");
            });

            this.ViewCommand = new Command<Visit>((visit) => {
                MessagingCenter.Send<ListScheduleViewModel, Visit>(this, "ViewScheduleMessage", visit);
            });
        }

        public async Task LoadScheduleList()
        {
            this.ModelList.Clear();
            this.Loading = true;

            // Visits current Day
            var scheduleList = await this.HttpService.ListAsync();
            scheduleList.ForEach(this.ModelList.Add);

            // Schedule by Week
            var groupedWeek = this.GetVisitsGroupedByWeek(scheduleList);
            groupedWeek.ForEach(this.ScheduleWeekList.Add);

            // Schedule by Month
            var groupedMonth = this.GetVisitsGroupedByMonth(scheduleList);
            groupedMonth.ForEach(this.ScheduleMonthList.Add);

            this.Loading = false;
        }


        private List<ScheduleWeek> GetVisitsGroupedByWeek(List<Visit> visits)
        {
            DateTime startWeekDay, endWeekDay;
            DateHelper.GetWeekRange(out startWeekDay, out endWeekDay);

            return visits.Where(v => v.ScheduleDate >= startWeekDay && v.ScheduleDate <= endWeekDay)
                         .GroupBy(v => v.ScheduleDate.ToString("yyyy-MM-dd"))
                         .Select(gv => new ScheduleWeek(gv)).ToList();
        }

        private List<ScheduleMonth> GetVisitsGroupedByMonth(List<Visit> visits)
        {
            return visits.GroupBy(v => v.ScheduleDate.ToString("MM"))
                              .Select(gv => new ScheduleMonth(gv)).ToList();
        }
    }
}
