﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using evisitors.Models;
using evisitors.Services;
using evisitors.Services.MockData;
using Xamarin.Forms;

namespace evisitors.ViewModels
{
    public class ListClientViewModel : BaseListViewModel<Client>
    {
        private List<Client> modelListFull;

        private string search;
        public string Search{
            get { return search; }
            set { 
                search = value; 
                this.ModelList.Clear();
                if (!String.IsNullOrEmpty(search))
                {
                    var filteredQuotes = this.modelListFull.Where(c => c.Name.ToLower().Contains(search.ToLower())).ToList();
                    filteredQuotes.ForEach(this.ModelList.Add);
                }
                else
                {
                    this.modelListFull.ForEach(this.ModelList.Add);
                }
            }
        }

        public ListClientViewModel()
        {
            this.HttpService = new ClientMockData();
            this.ModelList = new ObservableCollection<Client>();
            this.BindCommands();
        }

        private void BindCommands()
        {
            this.EditCommand = new Command<Client>((client) => {
                MessagingCenter.Send<ListClientViewModel, Client>(this, "EditClientMessage", client);
            });

            this.DeleteCommand = new Command<Client>(async (client) => {
                if (await this.HttpService.DeleteAsync(client))
                    MessagingCenter.Send<ListClientViewModel, MessageData>(this, "ClientMessage", new MessageData() { Success = true, Message = "Cliente excluido com sucesso!" } );
                else
                    MessagingCenter.Send<ListClientViewModel, MessageData>(this, "ClientMessage", new MessageData() { Success = false, Message = "Não foi possível excluir o cliente!" });
            });

            this.AddCommand = new Command(()=>{
                MessagingCenter.Send<ListClientViewModel>(this, "AddClientMessage");   
            });

            this.ViewCommand = new Command<Client>((client) => {
                MessagingCenter.Send<ListClientViewModel, Client>(this, "ViewClientMessage", client);
            });
        }

        public async Task LoadClientListProp()
        {
            this.ModelList.Clear();
            this.Loading = true;
            this.modelListFull = await this.HttpService.ListAsync();
            this.modelListFull.ForEach(this.ModelList.Add);
            this.Loading = false;
        }
    }
}

