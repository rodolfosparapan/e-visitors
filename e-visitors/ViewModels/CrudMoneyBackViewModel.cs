﻿using System;
using System.Windows.Input;
using evisitors.Models;
using evisitors.Services;
using Xamarin.Forms;

namespace evisitors.ViewModels
{
    public class CrudMoneyBackViewModel : BaseCrudViewModel<MoneyBack>
    {
        private MoneyBack currentMoneyBack;
        #region Propriedades

        #region MoneyBackData
        public string Descricao{
            get{ return currentMoneyBack.Descricao; }
            set{ currentMoneyBack.Descricao = value; 
                ((Command)this.SaveCommand).ChangeCanExecute();}
        }

        public string Tpsolicitacao
        {
            get{ return currentMoneyBack.Tpsolicitacao; }
            set{ currentMoneyBack.Tpsolicitacao = value; 
                ((Command)this.SaveCommand).ChangeCanExecute();}
        }


        public string NumRecibo
        {
            get { return currentMoneyBack.NumRecibo; }
            set { currentMoneyBack.NumRecibo = value; }
        }


        public string DtOcorrencia
        {
            get{ return currentMoneyBack.DtOcorrencia; }
            set{ currentMoneyBack.DtOcorrencia = value; }
        }

        public float Valor{
            get{ return currentMoneyBack.Valor; }
            set{ currentMoneyBack.Valor = value; }
        }
        #endregion MoneyBackData


        #endregion Propriedades

        public CrudMoneyBackViewModel()
        {
            IsAddOperation = true;
            PageTitle = "Novo Reembolso";
            currentMoneyBack = new MoneyBack();
            //HttpService = new MoneyBackMockData();
            this.BindingCommands();
        }

        public CrudMoneyBackViewModel(MoneyBack moneyback)
        {
            PageTitle = moneyback.NumRecibo;
            currentMoneyBack = moneyback;
            //HttpService = new MoneyBackMockData();
            this.BindingCommands();
        }

        private void BindingCommands()
        {
            SaveCommand = new Command(CommitMoneyBack, CanSubmitMoneyBack);
            EditCommand = new Command(() => OnlyView = false, CanSubmitMoneyBack);
        }

        private bool CanSubmitMoneyBack()
        {
            return !String.IsNullOrEmpty(Descricao) && !String.IsNullOrEmpty(DtOcorrencia);
        }

        private void CommitMoneyBack()
        {
            /*
            if (IsAddOperation) // Create MoneyBack
            {
                if (await HttpService.AddAsync(currentMoneyBack))
                    MessagingCenter.Send<CrudMoneyBackViewModel, MessageData>(this, "SaveMoneyBackMessage", new MessageData() { Success= true, Message = "MoneyBacke Inserido com Sucesso!"});
                else
                    MessagingCenter.Send<CrudMoneyBackViewModel, MessageData>(this, "SaveMoneyBackMessage", new MessageData() { Success = false, Message = "Não foi poss[ivel Inserir o MoneyBacke :(" });
            }
            else // Update MoneyBack
            {
                if (await HttpService.UpdateAsync(currentMoneyBack))
                    MessagingCenter.Send<CrudMoneyBackViewModel, MessageData>(this, "SaveMoneyBackMessage", new MessageData() { Success = true, Message = "MoneyBacke editado com sucesso!" });
                else
                    MessagingCenter.Send<CrudMoneyBackViewModel, MessageData>(this, "SaveMoneyBackMessage", new MessageData() { Success = false, Message = "Não foi possível editar o moneybacke :(" });
            }*/
        }
    }
}
