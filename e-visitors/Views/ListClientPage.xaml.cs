﻿using System;
using System.Collections.Generic;
using evisitors.Models;
using evisitors.ViewModels;
using Xamarin.Forms;

namespace evisitors.Views
{
    public partial class ListClientPage : ContentPage
    {
        public ListClientViewModel ViewModel { get; set; }

        public ListClientPage()
        {
            InitializeComponent();
            this.ViewModel = new ListClientViewModel();
            BindingContext = this.ViewModel;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<ListClientViewModel, MessageData>(this, "ClientMessage", (viewModel, objMessage) => {
                DisplayAlert("Informação", objMessage.Message, "OK");
                if (objMessage.Success)
                    LoadClientList();
            });

            MessagingCenter.Subscribe<ListClientViewModel, Client>(this, "ViewClientMessage", (viewModel, client) => {
                var page = new CrudClientPage(client);
                page.SetOnlyView();
                Navigation.PushAsync(page);
            });

            MessagingCenter.Subscribe<ListClientViewModel, Client>(this, "EditClientMessage", (viewModel, client) => {
                Navigation.PushAsync(new CrudClientPage(client));
            });

            MessagingCenter.Subscribe<ListClientViewModel>(this, "AddClientMessage", (viewModel) => {
                Navigation.PushAsync(new CrudClientPage());
            });

            LoadClientList();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<ListClientViewModel, MessageData>(this, "ClientMessage");
            MessagingCenter.Unsubscribe<ListClientViewModel, Client>(this, "ViewClientMessage");
            MessagingCenter.Unsubscribe<ListClientViewModel, Client>(this, "EditClientMessage");
            MessagingCenter.Unsubscribe<ListClientViewModel>(this, "AddClientMessage");
        }

        private async void LoadClientList()
        {
            await this.ViewModel.LoadClientListProp();
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
