﻿using System;
using System.Threading.Tasks;
using evisitors.Exceptions;
using Plugin.Media;
using Plugin.Media.Abstractions;

namespace evisitors.CrossDevice
{
    public static class Galery
    {
        public async static Task<MediaFile> PickPhoto()
        {
            if (!CrossMedia.Current.IsTakePhotoSupported)
                throw new GaleryException();

            var pickPhotoconfig = new PickMediaOptions()
            {
                PhotoSize = PhotoSize.Small
            };
            return await CrossMedia.Current.PickPhotoAsync(pickPhotoconfig);
        }
    }
}
