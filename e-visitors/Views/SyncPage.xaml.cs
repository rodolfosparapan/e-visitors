﻿using Eproposta.Services;
using evisitors.Database;
using evisitors.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Xamarin.Forms;

namespace evisitors.Views
{
    public partial class SyncPage : ContentPage
    {
        public SyncPage()
        {
            InitializeComponent();




            btSyncConfig.Clicked += delegate
            {

                ReasonDB db = new ReasonDB();
                /*
                Reason r = new Reason() {
                    Id = 3,
                    Description  = "Atirar",
                    Index = 1,
                    DtSync = "2018-02-09 00:00:00"
                };

                db.InsertReason(r);

                r = new Reason()
                {
                    Id = 4,
                    Description = "Matar",
                    Index = 2,
                    DtSync = "2018-02-08 00:00:00"
                };

                db.InsertReason(r);

                listaResultados.ItemsSource = db.GetReasons();
                */


                listaResultados.ItemsSource = db.GetReasons();

                ApiCall apiCall = new ApiCall();

                

                //Aqui buscamos os 10 com as maiores Notas e Iniciamos uma Thread
                apiCall.GetResponse<List<Reason>>("get", "motivos").ContinueWith(t =>
                {
                    //O ContinueWith é responsavel por fazer algo após o request finalizar

                    //Aqui verificamos se houve problema ne requisição
                    if (t.IsFaulted)
                    {
                        Debug.WriteLine("errorrrrrrrrrrr: " + t.Exception.Message);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            DisplayAlert("Falha", "Ocorreu um erro na Requisição :(", "Ok");

                            Debug.WriteLine("fodeu");
                            DisplayAlert("Falha", "Ocorreu um erro na Requisição 2 :(", "Ok");



                        });
                    }
                    //Aqui verificamos se a requisição foi cancelada por algum Motivo
                    else if (t.IsCanceled)
                    {
                        Debug.WriteLine("Requisição cancelada");

                        Device.BeginInvokeOnMainThread(() =>
                        {
                            DisplayAlert("Cancela", "Requisição Cancelada :O", "Ok");
                        });
                    }
                    //Caso a requisição ocorra sem problemas, cairemos aqui
                    else
                    {
                        //Se Chegarmos aqui, está tudo ok, agora itemos tratar nossa Lista
                        //Aqui Usaremos a Thread Principal, ou seja, a que possui as references da UI
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            //listaResultados.ItemsSource = t.Result;


                            db.SetAllReason(t.Result);

                            //listaResultados.ItemsSource = db.GetReasons();

                            //DisplayAlert("Carregado", "Requisição Ok ;D", "Ok");
                        });

                    }
                });



            };

        }
    }
}
