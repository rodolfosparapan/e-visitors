﻿using System;
using evisitors.Models;
using Xamarin.Forms;

namespace evisitors.ViewModels
{
    public class ViewScheduleViewModel : BaseViewModel
    {
        private string dayOfTheWeek;
        public string DayOfTheWeek
        {
            get
            {
                return dayOfTheWeek;
            }
            set
            {
                dayOfTheWeek = value;
                OnPropertyChanged();
            }
        }

        private string customers;
        public string Customers { 
            get
            {
                return customers;
            }
            set
            {
                customers = value;
                OnPropertyChanged();
            } 
        }

        public ViewScheduleViewModel(ScheduleWeek Schedule)
        {
            this.DayOfTheWeek = Schedule.DayOfTheWeek;
            this.Customers = Schedule.CustomersList ;
        }

        public ViewScheduleViewModel()
        {
        }
    }
}