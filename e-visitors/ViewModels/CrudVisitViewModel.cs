﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using evisitors.Database;
using evisitors.Models;
using evisitors.Services;
using evisitors.Services.MockData;
using Xamarin.Forms;

namespace evisitors.ViewModels
{
    public class CrudVisitViewModel : BaseCrudViewModel<Visit>
    {
        private Visit currentVisit;

        #region Proprierts
        public DateTime ScheduleDate { 
            get{ return currentVisit.ScheduleDate; } 
            set{ currentVisit.ScheduleDate = value;
                ((Command)this.SaveCommand).ChangeCanExecute();} 
        }

        public DateTime PerformedDate { 
            get{ return currentVisit.PerformedDate; } 
            set { currentVisit.PerformedDate = value; } 
        }

        // Substituir por um combo com pesquisa posteriormente
        public string Client { 
            get { return currentVisit.Client.Name; }
            set { currentVisit.Client.Name = value;
                ((Command)this.SaveCommand).ChangeCanExecute();}
        }

        // Posteriormente, trocar por tabela cadastrada no portal
        public ObservableCollection<VisitReason> VisitReasonList { get; set; }
        public VisitReason VisitReason{
            get { return currentVisit.VisitReason; } 
            set { currentVisit.VisitReason = value; } 
        }

        public int ProposalNumber {
            get { return currentVisit.Proposal.Number; } 
            set { currentVisit.Proposal.Number = value; } 
        }

        public double ProposalValue {
            get { return currentVisit.Proposal.Value; } 
            set { currentVisit.Proposal.Value = value; } 
        }

        // Posteriormente, trocar por tabela cadastrada no portal
        public ObservableCollection<VisitResult> VisitResultList { get; set; }
        public VisitResult VisitResult {
            get { return currentVisit.VisitResult; } 
            set { currentVisit.VisitResult = value; } 
        }

        public string Comments {
            get { return currentVisit.Comments; } 
            set { currentVisit.Comments = value; } 
        }

        public string ContactName {
            get { return currentVisit.Contact.Name; } 
            set { currentVisit.Contact.Name = value; } 
        }

        public string ContactDepartment {
            get { return currentVisit.Contact.Department; } 
            set { currentVisit.Contact.Department = value; } 
        }

        public string ContactPhone {
            get { return currentVisit.Contact.Phone; } 
            set { currentVisit.Contact.Phone = value; } 
        }
        #endregion Proprierts

        public CrudVisitViewModel()
        {
            IsAddOperation = true;
            PageTitle = "Nova Visita";
            currentVisit = new Visit();
            this.init();
        }

        public CrudVisitViewModel(Visit visit)
        {
            PageTitle = visit.ScheduleDate.ToString("dd/mm/YYYY");
            currentVisit = visit;
            this.init();
        }

        private void init()
        {
            HttpService = new VisitMockData();
            this.BindingCommands();
            this.LoadObservableCollections();
        }

        private void BindingCommands()
        {
            SaveCommand = new Command(CommitVisit, CanSubmitVisit);
            EditCommand = new Command(() => { IsFildsEnable = true; }, CanSubmitVisit);
        }

        private bool CanSubmitVisit()
        {
            return ScheduleDate != null && !String.IsNullOrEmpty(Client);
        }

        private async void CommitVisit()
        {
            if (IsAddOperation) // Create Visit
            {
                if (await HttpService.AddAsync(currentVisit))
                    MessagingCenter.Send<CrudVisitViewModel, MessageData>(this, "SaveVisitMessage", new MessageData() { Success = true, Message = "Visita inserida com Sucesso!" });
                else
                    MessagingCenter.Send<CrudVisitViewModel, MessageData>(this, "SaveVisitMessage", new MessageData() { Success = false, Message = "Não foi possível Inserir a visita :(" });
            }
            else // Update Client
            {
                if (await HttpService.UpdateAsync(currentVisit))
                    MessagingCenter.Send<CrudVisitViewModel, MessageData>(this, "SaveVisitMessage", new MessageData() { Success = true, Message = "Visita editada com sucesso!" });
                else
                    MessagingCenter.Send<CrudVisitViewModel, MessageData>(this, "SaveVisitMessage", new MessageData() { Success = false, Message = "Não foi possível editar a visita :(" });
            }
        }

        private void LoadObservableCollections()
        {
            VisitReasonList = new ObservableCollection<VisitReason>();
            var visitReasonList = VisitListMockData.ListReason;
            visitReasonList.ForEach(VisitReasonList.Add);

            /*var reasonList = (new ReasonDB()).GetReasons();

            foreach (var r in reasonList)
            {
                VisitReasonList.Add(new VisitReason() { CodReason = r.Id, Reason = r.Description });
            }*/



            VisitResultList = new ObservableCollection<VisitResult>();
            var visitResultList = VisitListMockData.ListResult;
            visitResultList.ForEach(VisitResultList.Add);
        }
    }
}
