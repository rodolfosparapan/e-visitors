﻿using System;
using System.Windows.Input;
using Xamarin.Forms;

// Fonte: https://stackoverflow.com/questions/26040652/binding-to-listview-item-tapped-property-from-view-model
namespace evisitors.CustomControls
{
    public class ListView : Xamarin.Forms.ListView
    {
        public static readonly BindableProperty ItemClickCommandProperty = BindableProperty.Create("ItemClickCommand", typeof(object), typeof(ListView), null, BindingMode.Default);

        public ListView()
        {
            this.ItemTapped += this.OnItemTapped;
        }

        public ICommand ItemClickCommand
        {
            get { return (ICommand)this.GetValue(ItemClickCommandProperty); }
            set { this.SetValue(ItemClickCommandProperty, value); }
        }

        private void OnItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item != null && this.ItemClickCommand != null && this.ItemClickCommand.CanExecute(e))
            {
                this.ItemClickCommand.Execute(e.Item);
                this.SelectedItem = null;
            }
        }
    }
}
