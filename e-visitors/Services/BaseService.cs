﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using evisitors.Services.ParseData;

namespace evisitors.Services
{
    public abstract class BaseService
    {
        protected HttpClient httpClient;
        protected const string serverUri = "http://www.e-visitors.com.br/ws/";

        public BaseService()
        {
            httpClient = new HttpClient();
        }
    }
}
