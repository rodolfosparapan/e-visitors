﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using evisitors.Models;

namespace evisitors.Services
{
    public class ClientService : BaseService, IService<Client>
    {
        public Task<bool> AddAsync(Client item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteAsync(Client item)
        {
            throw new NotImplementedException();
        }

        public Task<List<Client>> ListAsync()
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateAsync(Client item)
        {
            throw new NotImplementedException();
        }
    }
}
