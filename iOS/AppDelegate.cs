﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using SegmentedControl.FormsPlugin.iOS;
using UIKit;

namespace evisitors.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();

            // SegmentedControl Plugin
            SegmentedControlRenderer.Init();

            // Iconize Plugin
            FormsPlugin.Iconize.iOS.IconControls.Init();
            Plugin.Iconize.Iconize.With(new Plugin.Iconize.Fonts.FontAwesomeModule());

            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }

        public override void ReceivedLocalNotification(UIApplication application, UILocalNotification notification)
        {
            if (UIApplication.SharedApplication.ApplicationState == UIApplicationState.Active)
                new UIAlertView(notification.AlertAction, notification.AlertBody, null, "OK", null).Show();
        }


        public override void HandleAction(UIApplication application, string actionIdentifier, NSDictionary remoteNotificationInfo, Action completionHandler)
        {
            if (UIApplication.SharedApplication.ApplicationState == UIApplicationState.Active)
                new UIAlertView("Notification Action", actionIdentifier, null, "OK", null).Show();
            
            base.HandleAction(application, actionIdentifier, remoteNotificationInfo, completionHandler);
        }
    }
}
