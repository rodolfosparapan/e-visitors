﻿using System;

using Xamarin.Forms;
using evisitors.Views;

namespace evisitors
{
    public partial class App : Application
    {
        public static bool UseMockDataStore = true;
        public static string BackendUrl = "https://localhost:5000";

        public App()
        {
            InitializeComponent();

            //MainPage = new LoginPage();
            MainPage = new MasterDetailView();
        }

        protected override void OnStart()
        {
           // base.OnStart();
            //MessagingCenter.Subscribe<LoginPage>(this, "SuccessLogin", (loginPage) => {
              //  MainPage = new MasterDetailView();
            //});
        }
    }
}
