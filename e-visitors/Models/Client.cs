﻿using System;

namespace evisitors.Models
{
    public class Client
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Contact { get; set; }
        public string Doc1 { get; set; }
        public string Doc2 { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string DtSync { get; set; }
        public Address Address { get; set; } 
        public Geolocation Location { get; set; }

        public string CreateDate { get; set; }
        public bool Active { get; set; }
        public string ChangeDate { get; set; }

        public Client(){
            Address = new Address();
            Location = new Geolocation();
        }
    }

    public class Address
    {
        public string Street { get; set; }
        public string Neighborhood { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Complement { get; set; }
    }
}

