﻿using System;
using System.Windows.Input;
using evisitors.Models;
using evisitors.Services;
using evisitors.Services.MockData;
using Xamarin.Forms;

namespace evisitors.ViewModels
{
    public class CrudClientViewModel : BaseCrudViewModel<Client>
    {
        private Client currentClient;
        #region Proprierts

        #region ClientData
        public string Name{
            get{ return currentClient.Name; }
            set{ currentClient.Name = value; 
                ((Command)this.SaveCommand).ChangeCanExecute();}
        }

        public string Contact{
            get{ return currentClient.Contact; }
            set{ currentClient.Contact = value; 
                ((Command)this.SaveCommand).ChangeCanExecute();}
        }

        public string Phone{
            get{ return currentClient.Phone; }
            set{ currentClient.Phone = value; }
        }

        public string Email{
            get{ return currentClient.Email; }
            set{ currentClient.Email = value; }
        }
        #endregion ClientData

        #region ClientAddress

        public string Street { 
            get { return currentClient.Address.Street; } 
            set { currentClient.Address.Street = value; } }

        public string Neighborhood { 
            get { return currentClient.Address.Neighborhood; } 
            set { currentClient.Address.Neighborhood = value; } }

        public string City { 
            get { return currentClient.Address.City; } 
            set { currentClient.Address.City = value; } }

        public string State { 
            get { return currentClient.Address.State; } 
            set { currentClient.Address.State = value; } }

        public string ZipCode { 
            get { return currentClient.Address.ZipCode; } 
            set { currentClient.Address.ZipCode = value; } }

        public string Complement { 
            get { return currentClient.Address.Complement; } 
            set { this.currentClient.Address.Complement = value; } }

        #endregion ClientAddress
        #endregion Proprierts

        public CrudClientViewModel()
        {
            IsAddOperation = true;
            PageTitle = "Novo Cliente";
            currentClient = new Client();
            HttpService = new ClientMockData();
            this.BindingCommands();
        }

        public CrudClientViewModel(Client client)
        {
            PageTitle = client.Name;
            currentClient = client;
            HttpService = new ClientMockData();
            this.BindingCommands();
        }

        private void BindingCommands()
        {
            SaveCommand = new Command(CommitClient, CanSubmitClient);
            EditCommand = new Command(() => { OnlyView = false; IsFildsEnable = true; }, CanSubmitClient);
        }

        private bool CanSubmitClient()
        {
            return !String.IsNullOrEmpty(Name) && !String.IsNullOrEmpty(Contact);
        }

        private async void CommitClient()
        {
            if (IsAddOperation) // Create Client
            {
                if (await HttpService.AddAsync(currentClient))
                    MessagingCenter.Send<CrudClientViewModel, MessageData>(this, "SaveClientMessage", new MessageData() { Success= true, Message = "Cliente Inserido com Sucesso!"});
                else
                    MessagingCenter.Send<CrudClientViewModel, MessageData>(this, "SaveClientMessage", new MessageData() { Success = false, Message = "Não foi poss[ivel Inserir o Cliente :(" });
            }
            else // Update Client
            {
                if (await HttpService.UpdateAsync(currentClient))
                    MessagingCenter.Send<CrudClientViewModel, MessageData>(this, "SaveClientMessage", new MessageData() { Success = true, Message = "Cliente editado com sucesso!" });
                else
                    MessagingCenter.Send<CrudClientViewModel, MessageData>(this, "SaveClientMessage", new MessageData() { Success = false, Message = "Não foi possível editar o cliente :(" });
            }
        }
    }
}
