﻿using System;
using System.Collections.Generic;
using evisitors.Models;
using evisitors.ViewModels;
using Xamarin.Forms;

namespace evisitors.Views
{
    public partial class CrudClientPage : ContentPage
    {
        public CrudClientViewModel ViewModel { get; set; }

        public CrudClientPage()
        {
            InitializeComponent();
            ViewModel = new CrudClientViewModel();
            BindingContext = ViewModel;
        }

        public CrudClientPage(Client client)
        {
            InitializeComponent();
            ViewModel = new CrudClientViewModel(client);
            BindingContext = ViewModel;
        }

        public void SetOnlyView()
        {
            ViewModel.SetOnlyView();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<CrudClientViewModel, MessageData>(this, "SaveClientMessage", async (viewModel, objMessage) => {
                await DisplayAlert("Cliente", objMessage.Message, "OK");
                if (objMessage.Success)
                    await Navigation.PopAsync();
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<CrudClientViewModel, MessageData>(this, "SaveClientMessage");
        }
    }
}
