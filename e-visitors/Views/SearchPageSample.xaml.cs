﻿using System;
using System.Collections.Generic;
using evisitors.CustomRenderers;
using evisitors.ViewModels;
using Xamarin.Forms;

namespace evisitors.Views
{
    public partial class SearchPageSample : SearchPage
    {
        public SearchPageSample()
        {
            BindingContext = new SearchPageSampleViewModel();
            InitializeComponent();
        }
    }
}
