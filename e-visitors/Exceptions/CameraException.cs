﻿using System;
namespace evisitors.Exceptions
{
    public class CameraException : Exception
    {
        public CameraException(){}

        public CameraException(string message) : base(message){}

        public override string ToString()
        {
            return "Can not access device Camera!";
        }
    }
}
