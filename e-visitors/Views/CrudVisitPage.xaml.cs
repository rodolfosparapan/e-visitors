﻿using System;
using System.Collections.Generic;
using evisitors.Models;
using evisitors.ViewModels;
using Xamarin.Forms;

namespace evisitors.Views
{
    public partial class CrudVisitPage : ContentPage
    {
        public CrudVisitViewModel ViewModel { get; set; }

        public CrudVisitPage()
        {
            InitializeComponent();
            ViewModel = new CrudVisitViewModel();
            BindingContext = ViewModel;
        }

        public CrudVisitPage(Visit visit)
        {
            InitializeComponent();
            ViewModel = new CrudVisitViewModel(visit);
            BindingContext = ViewModel;
        }

        public void SetOnlyView()
        {
            ViewModel.SetOnlyView();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<CrudVisitViewModel, MessageData>(this, "SaveVisitMessage", (viewModel, objMessage) => {
                DisplayAlert("Visita", objMessage.Message, "OK");
                if (objMessage.Success)
                    Navigation.PopAsync();
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<CrudVisitViewModel, MessageData>(this, "SaveVisitMessage");
        }
    }
}
