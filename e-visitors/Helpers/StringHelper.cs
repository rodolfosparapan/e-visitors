﻿using System;

namespace evisitors.Helpers
{
    public static class StringHelper
    {
        public static string ToFirstLetterUpper(this string text)
        {
            return String.Concat(text.Substring(0,1).ToUpper(), text.Substring(1));
        }
    }
}

