﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using evisitors.Models;

namespace evisitors.Services.MockData
{
    public class ClientMockData : BaseMockService<Client>
    {
        public ClientMockData()
        {
            if (!AlreadyLoaded)
            {
                ModelList = new List<Client>(){

                    new Client(){
                        Id = 1,
                        Name = "KSB",
                        Contact = "Carmelo",
                        Phone = "(11) 4567-0099",
                        Email = "contato@ksb.com.br",
                        Address = new Address(){
                            Street = "Rua do Bozo, n 0",
                            Neighborhood = "Calca Marrom",
                            City = "Fantasias",
                            State = "Las Vegas",
                            ZipCode = "13200-000",
                            Complement = "Aff"
                        }
                    },
                    new Client(){
                        Id = 2,
                        Name = "Itaú",
                        Contact = "Sônia",
                        Phone = "(11) 94547-0559",
                        Email = "contato@itau.com.br",
                        Address = new Address(){
                            Street = "Rua do Bozo, n 0",
                            Neighborhood = "Calca Marrom",
                            City = "Fantasias",
                            State = "Las Vegas",
                            ZipCode = "13200-000",
                            Complement = "Aff"
                        }
                    },
                    new Client(){
                        Id = 3,
                        Name = "Vivo",
                        Contact = "Alberto",
                        Phone = "(11) 4855-0055",
                        Email = "contato@vivo.com.br",
                        Address = new Address(){
                            Street = "Rua do Bozo, n 0",
                            Neighborhood = "Calca Marrom",
                            City = "Fantasias",
                            State = "Las Vegas",
                            ZipCode = "13200-000",
                            Complement = "Aff"
                        }
                    },
                    new Client(){
                        Id = 4,
                        Name = "Loreal",
                        Contact = "Lívia",
                        Phone = "(11) 48599-2588",
                        Email = "contato@loreal.com.br",
                        Address = new Address(){
                            Street = "Rua do Bozo, n 0",
                            Neighborhood = "Calca Marrom",
                            City = "Fantasias",
                            State = "Las Vegas",
                            ZipCode = "13200-000",
                            Complement = "Aff"
                        }
                    }
                };
                IncrementID = ModelList.Count() + 1;
                AlreadyLoaded = true;
            }
        }

        public override async Task<bool> AddAsync(Client model)
        {
            model.Id = IncrementID;
            ModelList.Add(model);
            IncrementID++;
            return await Task.FromResult(true);
        }

        public override async Task<bool> UpdateAsync(Client model)
        {
            var _item = ModelList.Where((Client arg) => arg.Id == model.Id).FirstOrDefault();
            ModelList.Remove(_item);
            ModelList.Add(model);

            return await Task.FromResult(true);
        }
    }
}
