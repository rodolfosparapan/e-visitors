﻿using System;
using Android.Content;
using Android.Views;
using Android.Widget;
using evisitors.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

// Fonte: https://alexdunn.org/2017/03/21/xamarin-tips-xamarin-forms-android-custom-tableview-section-titles/

[assembly: ExportRenderer(typeof(TableView), typeof(ColoredTableViewRenderer))]
namespace evisitors.Droid.Renderers
{
    public class ColoredTableViewRenderer : TableViewRenderer
    {
        public ColoredTableViewRenderer(Context context) : base(context) { }

        protected override TableViewModelRenderer GetModelRenderer(Android.Widget.ListView listView, TableView view)
        {
            return new CustomHeaderTableViewModelRenderer(Context, listView, view);
        }

        private class CustomHeaderTableViewModelRenderer : TableViewModelRenderer
        {
            private readonly TableView _coloredTableView;

            public CustomHeaderTableViewModelRenderer(Context context, Android.Widget.ListView listView, TableView view) : base(context, listView, view)
            {
                _coloredTableView = view as TableView;
            }

            public override Android.Views.View GetView(int position, Android.Views.View convertView, ViewGroup parent)
            {
                var view = base.GetView(position, convertView, parent);

                var element = GetCellForPosition(position);

                // section header will be a TextCell
                if (element.GetType() == typeof(TextCell))
                {
                    try
                    {
                        // Get the textView of the actual layout
                        var textView = ((((view as LinearLayout).GetChildAt(0) as LinearLayout).GetChildAt(1) as LinearLayout).GetChildAt(0) as TextView);

                        // get the divider below the header
                        var divider = (view as LinearLayout).GetChildAt(1);

                        // Set the color
                        textView.SetTextColor(Android.Graphics.Color.DarkGray);
                        textView.TextAlignment = Android.Views.TextAlignment.TextStart;
                        textView.SetAllCaps(true);
                        textView.Gravity = GravityFlags.Bottom;
                        textView.SetPadding(50,100,0,20);
                        textView.SetBackgroundColor(Android.Graphics.Color.ParseColor("#F5F5F5"));
                        divider.SetBackgroundColor(Android.Graphics.Color.LightGray);
                    }
                    catch (Exception) { }
                }

                return view;
            }
        }
    }

}
