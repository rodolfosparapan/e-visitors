﻿using System;
using System.Globalization;

namespace evisitors.Helpers
{
    public static class DateHelper
    {
        public static void GetWeekRange(out DateTime begining, out DateTime end)
        {
            var now = DateTime.Now;
            var cultureInfo = new CultureInfo("pt-BR");
            var firstDayOfWeek = cultureInfo.DateTimeFormat.FirstDayOfWeek;

            int offset = firstDayOfWeek - now.DayOfWeek;
            if (offset != 1)
            {
                DateTime weekStart = now.AddDays(offset);
                DateTime endOfWeek = weekStart.AddDays(6);
                begining = weekStart;
                end = endOfWeek;
            }
            else
            {
                begining = now.AddDays(-6);
                end = now;
            }
        }
    }
}

