﻿using Realms;
using System;
using System.Collections.Generic;
using System.Text;

namespace evisitors.Models
{
    public class MoneyBack: RealmObject
    {
        /*
         * 
         * Field	Type	Null	Key	Default	Extra,
            codreembolso	int(10)	NO	PRI	\N	auto_increment,
            dtsolicitacao	date	YES		\N	,
            dtocorrencia	date	YES		\N	,
            numrecibo	varchar(100)	YES		\N	,
            descricao	varchar(100)	YES		\N	,
            streembolso	varchar(100)	YES		\N	,
            tpsolicitacao	varchar(30)	YES		\N	,
            dtcadastro	varchar(100)	YES		\N	,
            dtalteracao	varchar(100)	YES		\N	,
            dtsinc	datetime	YES		\N	,
            obs	text	YES		\N	,
            valor	decimal(15\,2)	YES		\N	
         * 
         */

        [PrimaryKey]
        public int Codreembolso { get; set; }
        public string DtOcorrencia { get; set; }
        public string Descricao { get; set; }
        public string Tpsolicitacao { get; set; }
        public string Streembolso { get; set; }
        public string NumRecibo { get; set; }
        public string Obs { get; set; }
        public float Valor { get; set; }

    }
}
