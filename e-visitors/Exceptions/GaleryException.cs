﻿using System;
namespace evisitors.Exceptions
{
    public class GaleryException : Exception
    {
        public GaleryException() { }
        
        public GaleryException(string message) : base(message) {}

        public override string ToString()
        {
            return "Can not access device Galery!";
        }
    }
}
