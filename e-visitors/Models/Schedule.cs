﻿using System;
using System.Collections.Generic;
using System.Linq;
using evisitors.Helpers;

namespace evisitors.Models
{
    public class ScheduleWeek
    {
        public int Id { get; set; }
        public string DayOfTheWeek { get; set; }
        public string CustomersList { get; set; }

        public ScheduleWeek(){}

        public ScheduleWeek(IGrouping<string, Visit> gv)
        {
            this.DayOfTheWeek = gv.FirstOrDefault().ScheduleDate.ToString("dddd").ToFirstLetterUpper();
            this.CustomersList = String.Join(", ", gv.Select(v => v.Client.Name).ToArray());
        }
    }

    public class ScheduleMonth
    {
        public string Month { get; set; }
        public int VisitsCount { get; set; }

        public ScheduleMonth(IGrouping<string, Visit> gv)
        {
            Month = gv.FirstOrDefault().ScheduleDate.ToString("MMMMM").ToFirstLetterUpper();
            VisitsCount = gv.Count();
        }
    }
}