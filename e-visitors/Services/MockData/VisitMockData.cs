﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using evisitors.Models;

namespace evisitors.Services.MockData
{
    public class VisitMockData : BaseMockService<Visit>
    {
        public VisitMockData()
        {
            if (!AlreadyLoaded)
            {
                ModelList = new List<Visit>()
                {
                    new Visit(){
                        CodVisit = 1,
                        ScheduleDate = new DateTime(2018, 02, 23, 13, 0, 0),
                        Client = new Client() {Name = "KSB"},
                        Contact = new Contact() {Name = "Pedro"}
                    },
                    new Visit(){
                        CodVisit = 2,
                        ScheduleDate = new DateTime(2018, 02, 23, 15, 0, 0),
                        Client = new Client() {Name = "Vivo"},
                        Contact = new Contact() {Name = "Maria"}
                    },
                    new Visit(){
                        CodVisit = 3,
                        ScheduleDate = new DateTime(2018, 02, 25, 18, 0, 0),
                        Client = new Client() {Name = "Vivo"},
                        Contact = new Contact() {Name = "Dennis"}
                    },
                    new Visit(){
                        CodVisit = 4,
                        ScheduleDate = new DateTime(2018, 03, 02, 10, 0, 0),
                        Client = new Client() {Name = "Lorel"},
                        Contact = new Contact() {Name = "Marcia"}
                    },
                    new Visit(){
                        CodVisit = 5,
                        ScheduleDate = new DateTime(2018, 03, 01, 08, 30, 0),
                        Client = new Client() {Name = "Sadia"},
                        Contact = new Contact() {Name = "Pedro"}
                    },
                };

                IncrementID = ModelList.Count() + 1;
                AlreadyLoaded = true;
            }
        }

        public override async Task<bool> AddAsync(Visit model)
        {
            model.CodVisit = IncrementID;
            ModelList.Add(model);
            IncrementID++;
            return await Task.FromResult(true);
        }

        public override async Task<bool> UpdateAsync(Visit model)
        {
            var _item = ModelList.Where((Visit arg) => arg.CodVisit == model.CodVisit).FirstOrDefault();
            ModelList.Remove(_item);
            ModelList.Add(model);

            return await Task.FromResult(true);
        }
    }
}
