﻿using System;
using System.Collections.Generic;

namespace evisitors.Models
{
    public class Visit
    {
        public int CodVisit { get; set; }
        public DateTime ScheduleDate { get; set; }
        public DateTime PerformedDate { get; set; }
        public Client Client { get; set; }
        public VisitReason VisitReason { get; set; }
        public Proposal Proposal { get; set; }
        public VisitResult VisitResult { get; set; }
        public string Comments { get; set; }
        public Contact Contact { get; set; }
        public Geolocation Geolocation { get; set; }
        public string DtSync { get; set; }

        public Visit(){

            ScheduleDate = new DateTime();
            PerformedDate = new DateTime();
            Client = new Client();
            VisitReason = new VisitReason();
            Proposal = new Proposal();
            VisitResult = new VisitResult();
            Contact = new Contact();
            Geolocation = new Geolocation();
        }
    }

    public class Proposal
    {
        public int CodProposal { get; set; }
        public int Number { get; set; }
        public string Description { get; set; }
        public double Value { get; set; }
    }

    public class VisitReason
    {
        public int CodReason { get; set; }
        public string Reason { get; set; }
    }

    public class VisitResult
    {
        public int CodResult { get; set; }
        public string Result { get; set; }
    }

    public class Contact
    {
        public int ContactId { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }

    public static class VisitListMockData{

        public static List<VisitReason> ListReason = new List<VisitReason>()
        {
            new VisitReason(){ CodReason = 1, Reason ="Gostei desse maluco" },
            new VisitReason(){ CodReason = 2, Reason ="Tem que ir neh" },
            new VisitReason(){ CodReason = 3, Reason ="Não tenho nada para fazer" },
            new VisitReason(){ CodReason = 3, Reason ="Tem que comprar o leite das criança" }
        };

        public static List<VisitResult> ListResult = new List<VisitResult>()
        {
            new VisitResult(){ CodResult = 1, Result = "O cara comprou o baguio" },
            new VisitResult(){ CodResult = 2, Result = "Cliente mão de vaca" },
            new VisitResult(){ CodResult = 3, Result = "Cliente desmarcou. Safado ..." },
            new VisitResult(){ CodResult = 4, Result = "Succeso Manolas! :)" },
            new VisitResult(){ CodResult = 5, Result = "É cilada Bino" },
            new VisitResult(){ CodResult = 6, Result = "Deu ruim, sai correndo" }
        };
    }

}
