/*
SQLyog Community v12.3.1 (64 bit)
MySQL - 5.6.37-log : Database - evisitors
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `clientes` */

DROP TABLE IF EXISTS `clientes`;

CREATE TABLE `clientes` (
  `codcliente` int(10) NOT NULL AUTO_INCREMENT,
  `codempresa` int(10) DEFAULT NULL,
  `nomerazao` varchar(100) DEFAULT NULL,
  `contato` varchar(100) DEFAULT NULL,
  `cpfcnpj` varchar(20) DEFAULT NULL,
  `rgie` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `cep` varchar(10) DEFAULT NULL,
  `endereco` varchar(100) DEFAULT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `estado` varchar(2) DEFAULT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `dtcadastro` datetime DEFAULT NULL,
  `dtalteracao` datetime DEFAULT NULL,
  `ativo` char(1) DEFAULT 'N',
  `dtsinc` datetime DEFAULT NULL,
  PRIMARY KEY (`codcliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `coordenadas` */

DROP TABLE IF EXISTS `coordenadas`;

CREATE TABLE `coordenadas` (
  `usuario` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtcadastro` datetime DEFAULT NULL,
  `id` int(10) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `empresas` */

DROP TABLE IF EXISTS `empresas`;

CREATE TABLE `empresas` (
  `codempresa` int(10) NOT NULL AUTO_INCREMENT,
  `nomerazao` varchar(100) DEFAULT NULL,
  `cpfcnpj` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `senha` varchar(50) DEFAULT NULL,
  `rgie` varchar(20) DEFAULT NULL,
  `cep` varchar(10) DEFAULT NULL,
  `endereco` varchar(100) DEFAULT NULL,
  `complemento` varchar(100) DEFAULT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `cidade` varchar(100) DEFAULT NULL,
  `estado` varchar(2) DEFAULT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `dtcadastro` datetime DEFAULT NULL,
  `dtalteracao` datetime DEFAULT NULL,
  `ativo` char(1) DEFAULT 'N',
  `dtsinc` datetime DEFAULT NULL,
  PRIMARY KEY (`codempresa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `motivos` */

DROP TABLE IF EXISTS `motivos`;

CREATE TABLE `motivos` (
  `codmotivo` int(10) NOT NULL AUTO_INCREMENT,
  `codempresa` int(10) DEFAULT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `ordem` int(10) DEFAULT NULL,
  `ativo` char(1) DEFAULT 'S',
  `dtsinc` datetime DEFAULT NULL,
  PRIMARY KEY (`codmotivo`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Table structure for table `reembolsos` */

DROP TABLE IF EXISTS `reembolsos`;

CREATE TABLE `reembolsos` (
  `codreembolso` int(10) NOT NULL AUTO_INCREMENT,
  `dtsolicitacao` date DEFAULT NULL,
  `dtocorrencia` date DEFAULT NULL,
  `numrecibo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `streembolso` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tpsolicitacao` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtcadastro` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtalteracao` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtsinc` datetime DEFAULT NULL,
  PRIMARY KEY (`codreembolso`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `representantes` */

DROP TABLE IF EXISTS `representantes`;

CREATE TABLE `representantes` (
  `codrepresentante` int(10) NOT NULL AUTO_INCREMENT,
  `codempresa` int(10) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `dtcadastro` datetime DEFAULT NULL,
  `dtalteracao` datetime DEFAULT NULL,
  `ativo` char(1) DEFAULT 'N',
  `dtsinc` datetime DEFAULT NULL,
  PRIMARY KEY (`codrepresentante`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `resultados` */

DROP TABLE IF EXISTS `resultados`;

CREATE TABLE `resultados` (
  `codresultado` int(10) NOT NULL AUTO_INCREMENT,
  `codempresa` int(10) DEFAULT NULL,
  `descricao` varchar(100) DEFAULT NULL,
  `ordem` int(1) DEFAULT NULL,
  `ativo` char(1) DEFAULT 'S',
  `dtsinc` datetime DEFAULT NULL,
  PRIMARY KEY (`codresultado`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `visitas` */

DROP TABLE IF EXISTS `visitas`;

CREATE TABLE `visitas` (
  `codvisita` int(10) NOT NULL AUTO_INCREMENT,
  `dtvisita_plan` date DEFAULT '0000-00-00',
  `dtvisita_real` date DEFAULT '0000-00-00',
  `codcliente` int(10) DEFAULT NULL,
  `codmotivo` int(10) DEFAULT NULL,
  `motivo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `codresultado` int(10) DEFAULT NULL,
  `resultado` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `realizada` int(11) DEFAULT NULL COMMENT '0=nao 1=sim',
  `dtrealizada` datetime DEFAULT '0000-00-00 00:00:00',
  `proposta` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contato` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `depto` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telcontato` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vlestimado` decimal(15,2) DEFAULT NULL,
  `dtcadastro` datetime DEFAULT '0000-00-00 00:00:00',
  `dtalteracao` datetime DEFAULT '0000-00-00 00:00:00',
  `latitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dtsinc` datetime DEFAULT NULL,
  PRIMARY KEY (`codvisita`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Table structure for table `www_log` */

DROP TABLE IF EXISTS `www_log`;

CREATE TABLE `www_log` (
  `codlog` int(10) NOT NULL AUTO_INCREMENT,
  `codpessoa` int(10) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `data` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `obs` text,
  PRIMARY KEY (`codlog`)
) ENGINE=MyISAM AUTO_INCREMENT=4180 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
