﻿using evisitors.Models;
using Realms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace evisitors.Database
{
    class MoneyBackDB
    {
        private Realm db;

        public MoneyBackDB()
        {
            db = Helpers.Util.GetInstanceRealm();
        }

        public List<MoneyBack> GetMoneyBacks()
        {
            return db.All<MoneyBack>().ToList();
        }


        public MoneyBack GetOneMoneyBack(int pID)
        {

            return db.All<MoneyBack>().FirstOrDefault(r => r.Codreembolso == pID);

        }

        public void InsertMoneyBack(MoneyBack reason)
        {
            db.Write(() => {
                db.Add(reason, false);
            });
        }

        public void UpdateMoneyBack(MoneyBack reason)
        {
            db.Write(() => {
                db.Add(reason, true);
            });
        }

        public void DeleteMoneyBack(MoneyBack reason)
        {
            db.Write(() => {
                db.Remove(reason);
            });
        }


        public void SetAllMoneyBack(List<MoneyBack> reasons)
        {

            db.Write(() =>
            {
                foreach (MoneyBack r in reasons)
                {
                    //db.Remove(r);
                    db.Add(r, true);
                }
            });
        }

    }
}
