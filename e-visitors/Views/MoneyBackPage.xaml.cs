﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;

using Xamarin.Forms;
using evisitors.CrossDevice;
using evisitors.Exceptions;

namespace evisitors.Views
{
    public partial class MoneyBackPage : ContentPage
    {
        private MediaFile file;

        public MoneyBackPage()
        {
            InitializeComponent();

            btCamera.Clicked += async delegate { 
                try
                {
                    this.file  = await Camera.TakePhoto();    
                    this.SetImageSourceFromMediaFile();
                }
                catch(CameraException ex)
                {
                    this.ShowExceptionMessage(ex);
                }
            };

            btGaleria.Clicked += async delegate {
                try
                {
                    this.file = await Galery.PickPhoto();
                    this.SetImageSourceFromMediaFile();
                }
                catch (GaleryException ex)
                {
                    this.ShowExceptionMessage(ex);
                }
            };
        }

        private async void ShowExceptionMessage(Exception ex)
        {
            await DisplayAlert("Ops :(", ex.ToString(), "OK");
        }

        private void SetImageSourceFromMediaFile()
        {
            if (this.file != null)
                imgFoto.Source = ImageSource.FromStream(() => this.file.GetStream());
        }
    }
}
