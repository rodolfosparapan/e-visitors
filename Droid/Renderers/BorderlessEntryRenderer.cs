﻿using System;
using Android.Content;
using evisitors.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

//Fonte: https://alexdunn.org/2017/03/08/xamarin-forms-borderless-entry/

[assembly: ExportRenderer(typeof(Entry), typeof(BorderlessEntryRenderer))]
namespace evisitors.Droid.Renderers
{
    public class BorderlessEntryRenderer : EntryRenderer
    {
        public BorderlessEntryRenderer(Context context) : base(context){}

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            Control?.SetBackgroundColor(Android.Graphics.Color.Transparent);
        }
    }
}
