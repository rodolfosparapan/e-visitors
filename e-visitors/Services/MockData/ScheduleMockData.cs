﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using evisitors.Models;

namespace evisitors.Services.MockData
{
    public class ScheduleMockData : BaseMockService<ScheduleWeek>
    {
        public ScheduleMockData()
        {
            if (!AlreadyLoaded)
            {
                ModelList = new List<ScheduleWeek>(){

                    new ScheduleWeek(){ Id=1, DayOfTheWeek = "Segunda-Feira",
                        CustomersList = "KSB, Itau, Vivo"
                    },

                    new ScheduleWeek(){ Id=2, DayOfTheWeek = "Quarta-Feira",
                        CustomersList = "Bradesco, Bovespa, Bollhoff"
                    },

                    new ScheduleWeek(){ Id=3, DayOfTheWeek = "Sexta-Feira",
                        CustomersList = "Claro, Carrefour, Loreal, Jequiti"
                    },

                    new ScheduleWeek(){ Id=4, DayOfTheWeek = "Domingo",
                        CustomersList = "FMU, Fleury, Le Postiche"
                    },

                };
                IncrementID = ModelList.Count() + 1;
                AlreadyLoaded = true;
            }
        }

        public override async Task<bool> AddAsync(ScheduleWeek model)
        {
            model.Id = IncrementID;
            ModelList.Add(model);
            IncrementID++;
            return await Task.FromResult(true);
        }

        public override async Task<bool> UpdateAsync(ScheduleWeek model)
        {
            var _item = ModelList.Where((ScheduleWeek arg) => arg.Id == model.Id).FirstOrDefault();
            ModelList.Remove(_item);
            ModelList.Add(model);

            return await Task.FromResult(true);
        }
    }
}
